#!/usr/bin/env bash
# this script fixes android photo bursts.
#it searches and replaces files 
# name_BURST4.jpg ->  name_BURST004.jpg 
# name_BURST14.jpg ->  name_BURST014.jpg
#
# This way the first 10 pictures in the sequence are now correct

photodir="/data/data/com.termux/files/home/main/DCIM/Camera/"
file_pattern=".*BURST[0-9]\{1,2\}.jpg"
bursts=()

# find files matching pattern and add to array 
while IFS= read -r -d $'\0'; do 
bursts+=("$REPLY") 
done < <(find $photodir -regex $file_pattern -type f -print0)

#perform rename ob the files
for((i=0; i<${#bursts[@]};i++));
do
# create new number format fir current file
nformat=$(printf "%03d" $(echo ${bursts[i]} | sed "s/^\(.*BURST\)\(.*\).jpg$/\2/"))
name=$(basename $(echo ${bursts[i]} | sed "s/^\(.*BURST\)\(.*\).jpg$/\1/"))
# rename the file
mv ${bursts[i]} $(echo ${bursts[i]} | sed  "s;^\(.*BURST\)\(.*\).jpg$;${photodir}MY_$name$(echo $nformat).jpg;")
done
